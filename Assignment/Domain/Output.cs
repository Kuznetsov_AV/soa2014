﻿using Newtonsoft.Json;
using System;
using System.Linq;

namespace Domain.Entity
{
  [Serializable]
  [JsonObject("Output")]
  public class Output
  {
    [JsonProperty("SumResult")]
    public decimal SumResult { get; set; }

    [JsonProperty("MulResult")]
    public int MulResult { get; set; }

    [JsonProperty("SortedInputs")]
    public decimal[] SortedInputs { get; set; }

    public Output() { }

    public Output(Input input)
    {
      this.SumResult = input.Sums.Sum() * input.K;
      this.MulResult = input.Muls.Aggregate((x, p) => x * p);
      this.SortedInputs = input.Muls
        .Select(m => decimal.Parse(m.ToString()))
        .Union(input.Sums)
        .OrderBy(m => m)
        .ToArray();
    }
  }
}
