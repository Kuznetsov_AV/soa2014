﻿namespace HttpClient
{
  public enum MethodType
  {
    /// <summary>
    /// Метод GET.
    /// </summary>
    Get,

    /// <summary>
    /// Метод POST.
    /// </summary>
    Post
  }
}
