﻿using System.IO;
using System.Net;
using System.Text;

namespace HttpClient
{
  public class Client
  {
    /// <summary>
    /// Адрес.
    /// </summary>
    private string url;

    /// <summary>
    /// Порт.
    /// </summary>
    private string port;

    /// <summary>
    /// Отдать ответ задачи серверу.
    /// </summary>
    /// <param name="serializedObject">Сериализованный объект.</param>
    /// <returns>True - запрос создан, false - нет.</returns>
    public bool WriteAnswer(string serializedObject)
    {
      var response = (HttpWebResponse)GetResponse("writeAnswer", MethodType.Post, serializedObject);
      return response != null;
    }

    /// <summary>
    /// Получить входные данные для задачи.
    /// </summary>
    /// <returns>Возвращает входные данные.</returns>
    public string GetInputData()
    {
      var response = (HttpWebResponse)GetResponse("getInputData", MethodType.Get, null);
      return response == null ? string.Empty : new StreamReader(response.GetResponseStream(), Encoding.UTF8).ReadToEnd();
    }

    /// <summary>
    /// Проверить, находится ли сервер в рабочем состоянии.
    /// </summary>
    /// <returns>True - сервер доступен, false - нет.</returns>
    public bool Ping()
    {
      var response = (HttpWebResponse)GetResponse("ping", MethodType.Get, null);
      return response == null ? false : response.StatusCode == HttpStatusCode.OK;
    }

    /// <summary>
    /// Получить ответ.
    /// </summary>
    /// <param name="method">Метод.</param>
    /// <param name="body">Тело.</param>
    /// <returns>Возвращает ответ.</returns>
    private HttpWebResponse GetResponse(string method, MethodType type, string body)
    {
      body = body ?? string.Empty;
      var request = WebRequest.Create(string.Format("{0}:{1}/{2}", this.url, this.port, method));
      request.ContentLength = Encoding.UTF8.GetByteCount(body);
      request.Method = type == MethodType.Get ? "GET" : "POST";
      request.Timeout = 200;

      if (body.Length > 0)
        using (var stream = request.GetRequestStream())
          stream.Write(Encoding.UTF8.GetBytes(body), 0, (int)request.ContentLength);

      try
      {
        return (HttpWebResponse)request.GetResponse();
      }
      catch
      {
        return null;
      }
    }

    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="url">Адрес.</param>
    /// <param name="port">Порт.</param>
    public Client(string url, string port)
    {
      this.url = url;
      this.port = port;
    }
  }
}
