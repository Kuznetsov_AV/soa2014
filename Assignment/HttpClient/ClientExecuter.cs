﻿using Domain.Entity;
using Serialization;
using System;

namespace HttpClient
{
  public static class ClientExecuter
  {
    private const string url = "http://127.0.0.1";

    public static void Execute()
    {
      var port = Console.ReadLine();
      var client = new Client(url, port);

      while (!client.Ping()) ;

      var inputSerializationObject = string.Empty;
      while ((inputSerializationObject = client.GetInputData()) == string.Empty) ;

      var inputSerializer = SimpleSerializerFactory<Input>.CreateSerializer("Json");
      var outputSerializer = SimpleSerializerFactory<Output>.CreateSerializer("Json");
      var input = inputSerializer.Deserialize(inputSerializationObject);
      var outputSerializationObject = outputSerializer.Serialize(new Output(input));

      while (!client.WriteAnswer(outputSerializationObject)) ;
    }
  }
}
