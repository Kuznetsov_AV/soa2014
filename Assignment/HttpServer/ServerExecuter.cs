﻿using System;

namespace HttpServer
{
  public class ServerExecuter
  {
    private const string url = "http://127.0.0.1";

    public static void Execute()
    {
      var port = Console.ReadLine();
      new Server(string.Format("{0}:{1}/", url, port));
    }
  }
}
