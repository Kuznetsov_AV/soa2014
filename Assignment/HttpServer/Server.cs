﻿using Domain.Entity;
using Serialization;
using System.IO;
using System.Net;
using System.Reflection;
using System.Text;
using System.Linq;
using System;

namespace HttpServer
{
  public class Server
  {
    /// <summary>
    /// Прослушиватель протокола HTTP.
    /// </summary>
    private HttpListener listener;

    /// <summary>
    /// Предоставляет доступ к объектам запросов и ответов.
    /// </summary>
    private HttpListenerContext context;

    /// <summary>
    /// Тело запроса.
    /// </summary>
    private string requestBody;

    /// <summary>
    /// Служит признком того, что сервер работает.
    /// </summary>
    private void Ping()
    {
      this.SendResponse(null);
    }

    /// <summary>
    /// Остановить сервер.
    /// </summary>
    private void Stop()
    {
      this.SendResponse(null);
      this.listener.Stop();
    }

    /// <summary>
    /// Запросить ответ задачи.
    /// </summary>
    private void GetAnswer()
    {
      var inputSerializer = SimpleSerializerFactory<Input>.CreateSerializer("Json");
      var outputSerializer = SimpleSerializerFactory<Output>.CreateSerializer("Json");
      var input = inputSerializer.Deserialize(requestBody);
      this.SendResponse(outputSerializer.Serialize(new Output(input)));
    }

    /// <summary>
    /// Послать входные данные.
    /// </summary>
    private void PostInputData()
    {
      using (var streamReader = new StreamReader(context.Request.InputStream, context.Request.ContentEncoding))
        this.requestBody = streamReader.ReadToEnd();
    }

    /// <summary>
    /// Отправить ответ.
    /// </summary>
    /// <param name="body">Тело.</param>
    private void SendResponse(string body)
    {
      body = body ?? string.Empty;
      var response = this.context.Response;
      response.StatusCode = (int)HttpStatusCode.OK;
      response.ContentEncoding = Encoding.UTF8;
      response.ContentLength64 = Encoding.UTF8.GetByteCount(body);
      using (var stream = response.OutputStream)
        stream.Write(Encoding.UTF8.GetBytes(body), 0, (int)response.ContentLength64);
    }

    /// <summary>
    /// Конструктор.
    /// </summary>
    /// <param name="url">Адрес.</param>
    public Server(string url)
    {
      this.listener = new HttpListener();
      this.listener.Prefixes.Add(url);
      this.listener.Start();
      this.requestBody = string.Empty;

      while (listener.IsListening)
      {
        this.context = this.listener.GetContext();
        var methodName = this.context.Request.Url.LocalPath.Substring(1).ToLower();
        var method = this.GetType()
          .GetMethods(BindingFlags.NonPublic | BindingFlags.Instance)
          .FirstOrDefault(m => m.Name.ToLower() == methodName); 
        if (method != null)
          method.Invoke(this, new Type[] { });
        else
          this.SendResponse(null);
      }
    }
  }
}
