﻿using Serialization.Serializer;
using System;

namespace Serialization
{
  /// <summary>
  /// Какое-то подобие фабрики сериализаторов.
  /// </summary>
  /// <typeparam name="T">Тип объекта.</typeparam>
  public static class SimpleSerializerFactory<T>
  {
    /// <summary>
    /// Создать сериализатор.
    /// </summary>
    /// <param name="serializationType">Тип сериализации.</param>
    /// <returns>Возвращает сериализатор.</returns>
    public static ISerializer<T> CreateSerializer(string serializationType)
    {
      switch (serializationType)
      {
        case "Xml":
          return new XmlSerializer<T>();
        case "Json":
          return new JsonSerializer<T>();
        default:
          throw new Exception("Неподдерживаемый тип сериализации");
      }
    }
  }
}
