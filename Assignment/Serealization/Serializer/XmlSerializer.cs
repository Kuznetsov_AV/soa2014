﻿using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Serialization.Serializer
{
  /// <summary>
  /// Xml-сериализатор.
  /// </summary>
  /// <typeparam name="T">Тип объекта.</typeparam>
  public class XmlSerializer<T> : ISerializer<T>
  {
    public string Serialize(T obj)
    {
      var stringBuilder = new StringBuilder();
      var serializer = new XmlSerializer(typeof(T));
      using (var xmlWriter = XmlWriter.Create(stringBuilder, new XmlWriterSettings { OmitXmlDeclaration = true }))
      {
        serializer.Serialize(xmlWriter, obj, new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty }));
        return stringBuilder.ToString();
      }
    }

    public T Deserialize(string obj)
    {
      var serializer = new XmlSerializer(typeof(T));
      using (var stream = new MemoryStream(Encoding.UTF8.GetBytes(obj)))
        return (T)serializer.Deserialize(stream);
    }
  }
}
