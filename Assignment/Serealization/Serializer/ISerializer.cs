﻿namespace Serialization.Serializer
{
  /// <summary>
  /// Интерфейс сериализатора.
  /// </summary>
  /// <typeparam name="T">Тип объекта.</typeparam>
  public interface ISerializer<T>
  {
    /// <summary>
    /// Сериализовать объект.
    /// </summary>
    /// <param name="obj">Объект.</param>
    /// <returns>Возвращает сериалзованный объект.</returns>
    string Serialize(T obj);

    /// <summary>
    /// Десериализовать объект.
    /// </summary>
    /// <param name="obj">Объект.</param>
    /// <returns>Возвращает десериализованый объект.</returns>
    T Deserialize(string obj);
  }
}
