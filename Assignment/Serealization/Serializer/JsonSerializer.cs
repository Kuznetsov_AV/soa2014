﻿using Newtonsoft.Json;

namespace Serialization.Serializer
{
  /// <summary>
  /// Json-сериализатор.
  /// </summary>
  /// <typeparam name="T">Тип объекта.</typeparam>
  public class JsonSerializer<T> : ISerializer<T>
  {
    public string Serialize(T obj)
    {
      return JsonConvert.SerializeObject(obj);
    }

    public T Deserialize(string obj)
    {
      return JsonConvert.DeserializeObject<T>(obj);
    }
  }
}
